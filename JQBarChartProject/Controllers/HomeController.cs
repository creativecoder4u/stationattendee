﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JQBarChartProject.Controllers
{
    public class HomeController : Controller
    {
        string responseData = string.Empty;
        public ActionResult Index()
        {
            TempData["ListOfJsonData"] = null;
            TempData["CountOfRecordToSkip"] = null;
            Session["ListOfCompleteData"] = null;
            TempData["TimeStampFromAfterFirstLoad"] = null;
            TempData["ReLoadContentTime"] = Convert.ToString(ConfigurationManager.AppSettings["TimeInSecondsToReloadChart"]);
            TempData["ChartData"] = null;
            TempData.Keep();
            return View();
        }

        [HttpGet]
        public async Task<bool> LoadData()
        {
            using (var client = new HttpClient())
            {
                var url = ConfigurationManager.AppSettings["AccessUrl"];
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["AccessToken"]);
                responseData = await client.GetStringAsync(url);
                TempData["ChartData"] = responseData;

                string jsonData = JsonConvert.SerializeObject(responseData, Formatting.None);
                System.IO.File.WriteAllText(Server.MapPath("~/JsonData/jsondata.txt"), jsonData);
            }
            return true;
        }
        public async Task<JsonResult> ReadJson(long IntervalTime = 0, string strIsDDlChanged = "N")
        {
            long TimeInterval = 0;
            if (IntervalTime > 0)
            {
                TimeInterval = IntervalTime;
            }
            else
            {
                TimeInterval = Convert.ToInt64(ConfigurationManager.AppSettings["TimeInMinutesToFilterData"]);
            }
            if (strIsDDlChanged == "Y")
            {
                TempData["CountOfRecordToSkip"] = null;
                Session["ListOfJsonData"] = Session["ListOfCompleteData"];
                Session["DataFromDate"] = null;
                Session["DataToDate"] = null;
            }
            List<Attendee> lstData = new List<Attendee>();
            List<Attendee> lstCompleteListOfData = new List<Attendee>();
            List<GroupedData> attendee = new List<GroupedData>();
            List<string> UniqueHostNames = null;
            if (Session["ListOfJsonData"] != null)
            {
                lstData = (List<Attendee>)Session["ListOfJsonData"];
                lstCompleteListOfData = (List<Attendee>)Session["ListOfCompleteData"];

                //Read all the unique host name
                UniqueHostNames = lstCompleteListOfData.Select(x => x.RfidReaderHostName).Distinct().ToList();

                //Filtering data
                //if (TempData["CountOfRecordToSkip"] == null)
                //{
                //    TempData["CountOfRecordToSkip"] = 0;
                //}
                //else
                //{
                //    int intCountToSkip = Convert.ToInt32(TempData["CountOfRecordToSkip"]);
                //    int intCountToGet = lstData.Count;
                //    IEnumerable<Attendee> SkippedList = lstData.Skip(intCountToSkip).Take(intCountToGet);
                //    lstData = SkippedList.ToList();
                //}
            }
            else
            {
                responseData = Server.MapPath("~/JsonData/jsondata.txt");
                using (StreamReader r = new StreamReader(responseData))
                {
                    string json = r.ReadToEnd();
                    dynamic array = JsonConvert.DeserializeObject(json);
                    string strJson = Convert.ToString(array);
                    dynamic objData = Newtonsoft.Json.JsonConvert.DeserializeObject(strJson);
                    if (objData != null)
                    {
                        string strData = Convert.ToString(objData);
                        lstData = JsonConvert.DeserializeObject<List<Attendee>>(strData);

                        //To remove character T from FirstRead and LastRead field
                        for (int i = 0; i < lstData.Count; i++)
                        {
                            lstData[i].LastReadDT = Convert.ToDateTime(lstData[i].LastRead.Replace("T", " "));
                            lstData[i].FirstReadDT = Convert.ToDateTime(lstData[i].FirstRead.Replace("T", " "));
                        }
                        //End of To remove character T from FirstRead and LastRead field

                        //Read all the unique host name
                        UniqueHostNames = lstData.Select(x => x.RfidReaderHostName).Distinct().ToList();

                        //Order the json data on the basis of first read
                        lstData.OrderBy(x => x.FirstReadDT);
                        Session["ListOfCompleteData"] = lstData;

                        //Filtering data
                        //if (TempData["CountOfRecordToSkip"] == null)
                        //{
                        //    TempData["CountOfRecordToSkip"] = 0;
                        //}
                        //else
                        //{
                        //    int intCountToSkip = Convert.ToInt32(TempData["CountOfRecordToSkip"]);
                        //    int intCountToGet = lstData.Count;
                        //    IEnumerable<Attendee> SkippedList = lstData.Skip(intCountToSkip).Take(intCountToGet);
                        //    lstData = SkippedList.ToList();
                        //}
                    }
                }
            }

            if (lstData.Count > 0)
            {
                Session["ListOfJsonData"] = lstData;
                DateTime startTime;
                DateTime dtFirstDateTimeFromJson;
                DateTime dtLastDateTimeFromJson;
                DateTime dtAddingTimeInterval;

                if (Session["DataFromDate"] != null && Session["DataToDate"] != null)
                {
                    startTime = Convert.ToDateTime(Session["DataToDate"]);
                    dtFirstDateTimeFromJson = startTime;
                }
                else
                {
                    dtFirstDateTimeFromJson = lstData[0].FirstReadDT;
                    dtLastDateTimeFromJson = lstData[0].LastReadDT;
                    //DateTime dtAddingTimeInterval = dtLastDateTimeFromJson.AddMinutes(TimeInterval);

                    startTime = dtFirstDateTimeFromJson.Date;
                    startTime = startTime.AddHours(dtFirstDateTimeFromJson.Hour);
                }
                //DateTime dtAddingTimeInterval = dtLastDateTimeFromJson.AddMinutes(TimeInterval);
                dtAddingTimeInterval = startTime.AddMinutes(TimeInterval);

                //IEnumerable<Attendee> FilteredData = lstData.Where(x => (x.FirstReadDT >= startTime && x.LastReadDT < dtAddingTimeInterval) || (x.FirstReadDT <= startTime && x.LastReadDT >= dtAddingTimeInterval) || (x.FirstReadDT >= startTime && x.FirstReadDT < dtAddingTimeInterval && x.LastReadDT >= dtAddingTimeInterval) || (x.FirstReadDT <= startTime && x.LastReadDT <= dtAddingTimeInterval && x.LastReadDT > startTime));
                IEnumerable<Attendee> FilteredData = lstData.Where(x => x.FirstReadDT <= dtAddingTimeInterval && x.LastReadDT > startTime);
                int counter = FilteredData.Count();
                //if (FilteredData == null || FilteredData.Count() <= 0)
                //{

                //    var nextDate = lstData.Where(x => x.FirstReadDT >= dtAddingTimeInterval).FirstOrDefault();

                //    dtFirstDateTimeFromJson = nextDate.FirstReadDT;
                //    if (TimeInterval > 5)
                //    {
                //        startTime = dtFirstDateTimeFromJson.Date;
                //        startTime = startTime.AddHours(dtFirstDateTimeFromJson.Hour);
                //    }
                //    else startTime = dtFirstDateTimeFromJson;

                //    dtAddingTimeInterval = startTime.AddMinutes(TimeInterval);

                //    FilteredData = lstData.Where(x => x.LastReadDT < dtAddingTimeInterval);
                //    //FilteredData = lstData.Where(x => (x.FirstReadDT >= startTime && x.LastReadDT < dtAddingTimeInterval) || (x.FirstReadDT <= startTime && x.LastReadDT >= dtAddingTimeInterval) || (x.FirstReadDT >= startTime && x.FirstReadDT < dtAddingTimeInterval && x.LastReadDT >= dtAddingTimeInterval) || (x.FirstReadDT <= startTime && x.LastReadDT <= dtAddingTimeInterval && x.LastReadDT > startTime));

                //}
                Session["DataFromDate"] = startTime;
                Session["DataToDate"] = dtAddingTimeInterval;

                // TempData["CountOfRecordToSkip"] = Convert.ToInt32(TempData["CountOfRecordToSkip"]) + FilteredData.ToList().Count;
                // lstData = FilteredData.ToList();
                //End of filtering data

                //Grouping Data
                List<string> AvailableHostNameUnderSpecificTime = new List<string>();
                //var GroupData = lstData.GroupBy(x => x.RfidReaderHostName);
                var GroupData = lstData.Select(r => new { r.RfidReaderHostName }).Distinct();
                foreach (var g in GroupData)
                {
                    //These lines of code are just for testing that what are the RfidTagID,firstRead,LastRead we are getting
                    List<string> id = FilteredData.Where(x => x.RfidReaderHostName == g.RfidReaderHostName).Select(x => x.RfidTagID).Distinct().ToList();
                    List<DateTime> ldate = FilteredData.Where(x => x.RfidReaderHostName == g.RfidReaderHostName).Select(x => x.LastReadDT).ToList();
                    List<DateTime> fdate = FilteredData.Where(x => x.RfidReaderHostName == g.RfidReaderHostName).Select(x => x.FirstReadDT).ToList();
                    //End of testing code,We can remove that during publishing our code

                    int DistinctTagIdsByHostName = FilteredData.Where(x => x.RfidReaderHostName == g.RfidReaderHostName).Select(x => x.RfidTagID).Distinct().Count();
                    //var test = lstData.Select(r => new { r.RfidTagID, r.RfidReaderHostName }).Where(x => x.RfidReaderHostName == g.RfidReaderHostName).Distinct();
                    GroupedData objGData = new GroupedData();

                    objGData.HostName = g.RfidReaderHostName;
                    objGData.CountOfAttendee = DistinctTagIdsByHostName;//g.Count();
                    AvailableHostNameUnderSpecificTime.Add(g.RfidReaderHostName);
                    attendee.Add(objGData);
                }
                //End of grouping data

                //To find if there is any Host name that got missed after filter,if yes then we have to bind that with 0 value
                IEnumerable<string> UniqueHostNameAfterFilter = UniqueHostNames.Where(l1 => !AvailableHostNameUnderSpecificTime.Any(l2 => l1 == l2));
                foreach (var host in UniqueHostNameAfterFilter)
                {
                    GroupedData objGData = new GroupedData();
                    objGData.HostName = host;
                    objGData.CountOfAttendee = 0;
                    attendee.Add(objGData);
                }
                //End of find unique host name
            }
            else
            {
                lstData = null;
                Session["ListOfJsonData"] = null;
                Session["ListOfJsonData"] = null;
                Session["DataFromDate"] = null;
                Session["DataToDate"] = null;
            }

            var JsonData = Json(JsonConvert.SerializeObject(attendee.OrderBy(x => x.HostName)), JsonRequestBehavior.AllowGet);
            JsonData.MaxJsonLength = int.MaxValue;
            return JsonData;
        }
        public String GetCurrentTimeStamp()
        {
            string strTimeStamp = "";
            if (Session["DataFromDate"] != null && Session["DataToDate"] != null && TempData["TimeStampFromAfterFirstLoad"] == null)
            {
                strTimeStamp = Convert.ToString(Session["DataFromDate"]) + "," + Convert.ToString(Session["DataToDate"]);
                TempData["TimeStampFromAfterFirstLoad"] = Session["DataToDate"];
            }
            else
            {
                strTimeStamp = Convert.ToString(TempData["TimeStampFromAfterFirstLoad"]) + "," + Convert.ToString(Session["DataToDate"]);
            }
            return strTimeStamp;
        }
    }
    public class GroupedData
    {
        public int CountOfAttendee { get; set; }
        public string HostName { get; set; }
    }
    public class Attendee
    {
        public string RfidTagID { get; set; }
        public string FirstRead { get; set; }
        public string LastRead { get; set; }
        public string RfidReaderIP { get; set; }
        public string RfidReaderMacAddress { get; set; }
        public string RfidReaderHostName { get; set; }
        public string AntennaPosition { get; set; }
        public string Rssi { get; set; }
        public int Id { get; set; }
        public DateTime FirstReadDT { get; set; }
        public DateTime LastReadDT { get; set; }
    }

    public class ResultData
    {
        string JsonData { get; set; }
        string FromTime { get; set; }
        string ToTime { get; set; }

    }
}
